//
//  ResultViewController.swift
//  PersonalQuiz
//
//  Created by Alexey Efimov on 07.08.2023.
//

import UIKit

final class ResultViewController: UIViewController {
    
    @IBOutlet weak var animalTypeLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBAction func doneButtonPressed(_ sender: UIBarButtonItem) {
        dismiss(animated: true)
    }
    
    deinit {
        print("\(type(of: self)) has been deallocated")
    }
    
    var answers: [Answer]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.hidesBackButton = true
        let maxAnimal = getMaxAnimal()
        updateUI(with: maxAnimal)
    }
}

extension ResultViewController {
        private func getMaxAnimal() -> Animal? {
            return Dictionary(grouping: answers, by: { $0.animal })
                .sorted(by: { $0.value.count > $1.value.count })
                .first?.key
        }
        
        private func updateUI(with animal: Animal?) {
            animalTypeLabel.text = "Вы - \(animal?.rawValue ?? "🐶")!"
            descriptionLabel.text = animal?.definition ?? ""
        }
    }
